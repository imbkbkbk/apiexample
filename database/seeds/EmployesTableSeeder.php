<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employers = [
            ['email' => 'MockEmployee1@test'],
            ['email' => 'MockEmployee2@test'],
            ['email' => 'MockEmployee3@test'],
        ];

        DB::table('employers')->insert($employers);
    }
}
