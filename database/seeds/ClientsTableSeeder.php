<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = [
            ['email' => 'MockClient1@test'],
            ['email' => 'MockClient2@test'],
            ['email' => 'MockClient3@test'],
        ];

       DB::table('clients')->insert($clients);
    }
}
