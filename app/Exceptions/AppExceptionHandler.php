<?php
namespace App\Exceptions;

class AppExceptionHandler {
    private static $userErrors = [
      'EMPLOYEE_NOT_FOUND' => ['message' => 'Employee is not found', 'code' => 404],
      'CLIENT_NOT_FOUND' => ['message' => 'Client is not found', 'code' => 404],
      'TITLE_FIELD_REQUIRED' => ['message' => 'Field "title" is required', 'code' => 422],
      'DESCRIPTION_FIELD_REQUIRED' => ['message' => 'Field "description" is required', 'code' => 422],
    ];

    public static function handle(AppException $e){
        $error = self::$userErrors[$e->getUserMessage()];
        return response()->json(['message' => $error['message']], $error['code']);
    }
}
