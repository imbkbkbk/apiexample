<?php
namespace App\Exceptions;

class AppException extends \Exception{
    private $userMessage;

    public function __construct(string $message){
        $this->userMessage = $message;
        parent::__construct("App exception");
    }

    public function getUserMessage(): string{
        return $this->userMessage;
    }
}
