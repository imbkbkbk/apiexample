<?php

namespace App\Providers;

use App\Repositories;
use App\Repositories\Interfaces;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Interfaces\OrderRepository::class,
        Repositories\EloquentOrderRepository::class);

        $this->app->bind(Interfaces\EmployeeRepository::class,
        Repositories\EloquentEmployeeRepository::class);

        $this->app->bind(Interfaces\ClientRepository::class,
        Repositories\EloquentClientRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
