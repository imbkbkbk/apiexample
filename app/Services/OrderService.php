<?php

namespace App\Services;

use App\Dto\CreateOrderDTO;
use App\Exceptions\AppException;
use App\Repositories\Interfaces\ClientRepository;
use App\Repositories\Interfaces\EmployeeRepository;
use App\Repositories\Interfaces\OrderRepository;

class OrderService
{
    private OrderRepository $orderRepo;
    private ClientRepository $clientRepo;
    private EmployeeRepository $employeeRepo;

    public function __construct(
        OrderRepository $orderRepo,
        ClientRepository $clientRepo,
        EmployeeRepository $employeeRepo
    ) {
        $this->orderRepo = $orderRepo;
        $this->clientRepo = $clientRepo;
        $this->employeeRepo = $employeeRepo;
    }

    /**
     * Validate order data and create new order
     * @param  CreateOrderDTO  $orderDto
     * @return int
     * @throws AppException
     */
    public function create(CreateOrderDTO $orderDto): int
    {
        $client_id = $orderDto->getClientId();
        $title = $orderDto->getTitle();
        $description = $orderDto->getDescription();

        if ($title == ""){
            throw new AppException('TITLE_FIELD_REQUIRED');
        }

        if ($description == ""){
            throw new AppException('DESCRIPTION_FIELD_REQUIRED');
        }

        $client = $this->clientRepo->findById($client_id);
        if (count($client) == 0) {
            throw new AppException('CLIENT_NOT_FOUND');
        }

        return $this->orderRepo->insert($orderDto);
    }
}
