<?php

namespace App\Repositories;

use App\Dto\CreateOrderDTO;
use App\Models\Order;
use App\Repositories\Interfaces\OrderRepository;

class EloquentOrderRepository implements OrderRepository
{
    public function all(): array
    {
        return Order::all()->toArray();
    }

    /**
     * Insert Order to db and returns Order`s id
     * @param  CreateOrderDTO  $orderDto
     * @return int $orderId
     */
    public function insert(CreateOrderDTO $orderDto): int
    {
        $order = Order::create([
            'client_id' => $orderDto->getClientId(),
            'title' => $orderDto->getTitle(),
            'description' => $orderDto->getDescription()
        ]);

        $order->save();

        return $order->id;
    }
}
