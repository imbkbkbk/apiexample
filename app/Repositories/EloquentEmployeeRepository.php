<?php
namespace App\Repositories;

use App\Models\Employee;
use App\Repositories\Interfaces\EmployeeRepository;

class EloquentEmployeeRepository implements EmployeeRepository{
    public function all(): array {
        return Employee::all()->toArray();
    }

    public function findById(int $id): array{
        return Employee::all()->find($id)->toArray();
    }
}
