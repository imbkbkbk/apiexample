<?php
namespace App\Repositories\Interfaces;

use App\Dto\CreateOrderDTO;

interface EmployeeRepository {
    public function all(): array;
    public function findById(int $id): array;
}
