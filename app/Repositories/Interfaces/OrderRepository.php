<?php
namespace App\Repositories\Interfaces;

use App\Dto\CreateOrderDTO;

interface OrderRepository {
    public function all(): array;
    public function insert(CreateOrderDTO $orderDto): int;
}
