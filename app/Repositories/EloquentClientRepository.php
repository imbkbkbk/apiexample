<?php
namespace App\Repositories;

use App\Models\Client;
use App\Repositories\Interfaces\ClientRepository;

class EloquentClientRepository implements ClientRepository {
    public function all(): array{
        return Client::all()->toArray();
    }

    public function findById(int $id): array{
        if ($client =  Client::all()->find($id)){
            return $client->toArray();
        }
        return [];
    }
}
