<?php

namespace App\Dto;

/**
 * Class CreateOrderDTO
 * @package App\Dto
 */
class CreateOrderDTO
{
    private int $client_id;
    private string $title;
    private string $description;

    public function __construct(int $client_id, string $title, string $description)
    {
        $this->client_id = $client_id;
        $this->title = $title;
        $this->description = $description;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
