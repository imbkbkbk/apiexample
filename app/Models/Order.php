<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    protected $fillable = ['client_id', 'title', 'description'];
    public function employee(): BelongsTo
    {
        return $this->belongsTo('App\Models\Employee');
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo('App\Models\Employee');
    }

    public function contract(): HasMany
    {
        return $this->hasMany('App\Models\Contract');
    }
}
