<?php

namespace App\Http\Controllers;

use App\Dto\CreateOrderDTO;
use Illuminate\Http\Request;
use App\Services\OrderService;

class OrderController extends Controller
{
    private OrderService $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    public function create(Request $request)
    {
        $client_id = (int)$request->get('client_id');
        $description = (string)$request->get('description');
        $title = (string)$request->get('title');

        $orderDto = new CreateOrderDTO($client_id, $title, $description);

        $orderId = $this->service->create($orderDto);

        return response()->json(['message' => 'Order created successfully', 'order_id' => $orderId], 200);
    }
}
